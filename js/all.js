(function ($, app) {
    var homeCls = function () {
        this.run = function () {
            this.init();
            this.bindEvents();
        };
        
        this.init = function () {
            
        };
        
        this.bindEvents = function () {
            toggle_dropdown();
            toggle_play_pause();
            toggle_volume();
            // pagePilling_setup();
            // fullpage_setup();
            // aos_setup();
        };
        var toggle_dropdown= function () {
            $(".hasDropDown").bind("click", '*',function (e) {
                console.log("a");
                if (document.getElementById("menuDropdown").style.display=="none"){
                    document.getElementById("menuDropdown").style.display="block";
                    document.getElementById("icon_log").style.display="none";
                    document.getElementById("icon_menu").style.display="none";
                    document.getElementById("logo").style.display="none";
                    document.getElementById("icon_close").style.display="inline-block";
                } else {
                    document.getElementById("menuDropdown").style.display="none";
                    document.getElementById("icon_log").style.display="inline-block";
                    document.getElementById("icon_menu").style.display="inline-block";
                    document.getElementById("logo").style.display="block";
                    document.getElementById("icon_close").style.display="none";

                }
                e.stopPropagation();
            });
        };
        var toggle_play_pause = function () {
            var video = document.getElementById("video");
            $("#overlay").bind("click", function () {
                console.log(111);
                if (video.paused){
                    video.play();
                    document.getElementById("play_icon").style.display="none";
                    document.getElementById("overlay").style.opacity="0";
                }else {
                    video.pause();
                    document.getElementById("play_icon").style.display="block";
                    document.getElementById("overlay").style.opacity="0.5";
                }
            });
            $("#play_icon").bind("click", function () {
                console.log(111);
                if (video.paused){
                    video.play();
                    document.getElementById("play_icon").style.display="none";
                    document.getElementById("overlay").style.opacity="0";
                }else {
                    video.pause();
                    document.getElementById("play_icon").style.display="block";
                    document.getElementById("overlay").style.opacity="0.5";
                }
            });
        };
        var toggle_volume=function() {
            var video = document.getElementById("video");
            
            $("#mute").bind("click", function () {
                console.log(111);
                if (video.muted){
                    video.muted=!video.muted;
                    document.getElementById("mute").innerHTML='<i class="fas fa-volume-up"></i>';
                } else{
                    video.muted=!video.muted;
                    document.getElementById("mute").innerHTML='<i class="fas fa-volume-mute"></i>';
            
                }
            });
        };
        var fullpage_setup= function(){
            if (screen.width >1209){
                new fullpage('#fullpage', {
                    //options here
                    autoScrolling:true,
                    scrollHorizontally: true,
                    anchors:['firstPage', 'secondPage', 'thirdPage','fourthPage']
                });
            } else{
                console.log("a");
            }
            
        };
        var aos_setup= function() {
            AOS.init();
            // AOS.init({
            //     useClassNames: true,
            // }) ;
            
        };
        var pagePilling_setup = function(){
            $('#pagepiling').pagepiling();
        }
        
    };
        $(document).ready(function () {
            var homeObj = new homeCls();
            homeObj.run();

            
            
        });
}(jQuery, $.app));
var animation_inView= function(element) {
    // get the element to animate
    var elementHeight = element.clientHeight;

    // listen for scroll event and call animate function
    document.addEventListener('scroll', animate);

    // check if element is in view
    function inView() {
        // get window height
        var windowHeight = window.innerHeight;
        // get number of pixels that the document is scrolled
        var scrollY = window.scrollY || window.pageYOffset;
        
        // get current scroll position (distance from the top of the page to the bottom of the current viewport)
        var scrollPosition = scrollY + windowHeight;
        // get element position (distance from the top of the page to the bottom of the element)
        var elementPosition = element.getBoundingClientRect().top + scrollY + elementHeight;
        
        // is scroll position greater than element position? (is element in view?)
        if (scrollPosition >= elementPosition) {
            return true;
        }
        
        return false;
    }
    function animate() {
            // is element in view?
            if (inView()) {
                // element is in view, add class to element
                element.classList.add('active');
            }else{
                element.classList.remove('active');
            }
        }
    // animate element when it is in view
    
}
var video_inView= function(element) {
    var elementHeight = element.clientHeight;

    document.addEventListener('scroll', videoPlay);

    function inView() {
        // get window height
        var windowHeight = window.innerHeight;
        // get number of pixels that the document is scrolled
        var scrollY = window.scrollY || window.pageYOffset;
        
        // get current scroll position (distance from the top of the page to the bottom of the current viewport)
        var scrollPosition = scrollY + windowHeight;
        // get element position (distance from the top of the page to the bottom of the element)
        var elementPosition = element.getBoundingClientRect().top + scrollY + elementHeight;
        
        // is scroll position greater than element position? (is element in view?)
        if (scrollPosition >= elementPosition) {
            return true;
        }
        
        return false;
    }
    function videoPlay() {
        // is element in view?
        if (inView()) {
            // element is in view, add class to element
            element.play();
            document.getElementById("play_icon").style.display="none";
            document.getElementById("overlay").style.opacity="0";
        }else{
            element.pause();
            document.getElementById("play_icon").style.display="block";
            document.getElementById("overlay").style.opacity="0.5";
        }
    }
}
var check_video = function() {
    var video = document.getElementById("video");
    video_inView(video);
}

var check = function () {
    var elementList = document.getElementsByClassName('section');
    for (var i =0; i< elementList.length; i++) {
        animation_inView(elementList[i]);
    }
    
}
if (screen.width >1299){
    document.addEventListener('scroll', check);
    console.log(screen.width);

} else {
    console.log(12);
}

document.addEventListener('scroll', check_video);

